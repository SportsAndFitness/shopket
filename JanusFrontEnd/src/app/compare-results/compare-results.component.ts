import { Component, OnInit } from '@angular/core';
import {  HostBinding, Input, Output, EventEmitter, HostListener } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AppComponent } from '../app.component';
@Component({
  selector: 'app-compare-results',
  templateUrl: './compare-results.component.html',
  styleUrls: ['./compare-results.component.css']
})



export class CompareResultsComponent implements OnInit {
  @Input() Component;
  appComponent=new AppComponent();
  objectValue:any;

 
Getresponse: Object;
 
 
constructor(private http: HttpClient) {
  
 }

 searchText:any;
  

@Output() toggle : EventEmitter<string> = new EventEmitter<string>();
public showContent: boolean = false;
  
  ngOnInit(){
    setTimeout(()=>this.showContent=true, 3000);
    // // console.log(this.Component);
    if (this.Component!=null){
      this.GetResponseOfParticularProduct(this.objectValue);
    }
    if(this.Component==null){
      this.CloseSection();
    } 
  }

  ngOnChanges() {
    setTimeout(()=>this.showContent=true, 3000);
     if (this.Component!=null){
       this.GetResponseOfParticularProduct(this.objectValue);
    }
     if(this.Component==null){
      this.CloseSection();
    }
  
  }
  GetResponseOfParticularProduct(Component: any){
    this.searchText="";
    Component=this.Component;
    //call the api and save response to be served here
    this.http.get('http://beta.shopket.in/Search/JsonProductCompare?category=All&searchText='+this.searchText+'&title='+Component.key+'&productName='+Component.productName).subscribe((response) => {
           this.Getresponse = response;
          console.log(this.Getresponse);
  })
}
  CloseSection(){
    this.toggle.emit("ok");
    this.Getresponse=null;
  }
}

import { Component, OnInit, EventEmitter, Output, HostListener } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { log } from 'util';
@Component({
  selector: 'app-product-card',
  templateUrl: './product-card.component.html',
  styleUrls: ['./product-card.component.css']
})
export class ProductCardComponent implements OnInit {
  @Output() toggle: EventEmitter<null> = new EventEmitter();
  @Output() info: EventEmitter<string> = new EventEmitter<string>();
  constructor(private http: HttpClient) {
    
  }
  item: string;
  Getresponse: any;
  imgRef: any;
  title: any;
  Provider: any;
  searchText: any;
  product :any;



  ngOnInit() {
    this.searchText = "";
    this.http.get('http://beta.shopket.in/Search/jsonSearch?category=All&SearchText=' + this.searchText).subscribe((response) => {
      this.Getresponse = response;
      console.log(this.Getresponse);
    })
  }

  CompareProduct(key1:string,productName1: string) {
    this.Compare();
    this.product ={
                    "key": key1,
                    "productName": productName1
                  }
    this.info.emit(this.product);
  }
  @HostListener('click')
  click() {
    this.toggle.emit();
  }
  compareSectionIsOpened= false;
  Compare(){
    this.compareSectionIsOpened = !this.compareSectionIsOpened;
  }
}

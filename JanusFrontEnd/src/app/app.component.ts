import { Component } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatCardModule, MatIconModule, MatToolbarModule, MatButtonModule, MatFormFieldModule, MatInputModule } from '@angular/material';
import {  OnInit, HostBinding, Input, Output, EventEmitter, HostListener } from '@angular/core';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    
    BrowserModule,
    BrowserAnimationsModule,
    MatCardModule,
    MatIconModule,
    MatToolbarModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    
  ],
  providers: [],
  bootstrap: [AppComponent]
})

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  Component:any=null;
  title = "Shopket";  
  
  name:any;
  ngOnInit() {
    this.compareSectionIsOpened = false;
  }
  compareSectionIsOpened = false;

  toggleCompareSection() {
    this.compareSectionIsOpened = !this.compareSectionIsOpened;
  }
  infoObject(Component)
  {

    this.Component=Component;
  }
  CompareProd(ProdName: string){
    this.Component=ProdName;
    this.name = ProdName;
    this.compareSectionIsOpened = !this.compareSectionIsOpened;
  }
}
